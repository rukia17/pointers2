#include <iostream>

using namespace std;




int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;

cout << "X value "<< x << "X address "<< &x <<endl;
cout << "A value "<< a << "A address "<< &a <<endl;
cout << "B value "<< b << "B address "<< &b <<endl;
cout << "C value "<< c << "C address "<< &c <<endl;
cout << "D value "<< d << "D address "<< &d;



}


